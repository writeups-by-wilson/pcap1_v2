```
┌──(kali㉿kali)-[~/Desktop/practice_ctfs/pcap1_v2]
└─$ ls -l                      
total 11288
-rw-r--r-- 1 kali kali      31 Jul 15  2019 pcap1_desc.txt
-rw-r--r-- 1 kali kali 2309752 Jul 21  2019 pcap1_v2.pcapng
-rw-rw-rw- 1 kali kali 2310087 Aug 22 23:05 pcap1_v2.zip

┌──(kali㉿kali)-[~/Desktop/practice_ctfs/pcap1_v2]
└─$ cat pcap1_desc.txt                    
Find the flag.
Format: CCT{.*}
```

Upon opening the PCAP file with Wireshark, I am greeted with unfamiliar USB device data. Research suggests that `usb.capdata` is what we should be focusing on.

First, let's start by isolating the capture data:
`tshark -r pcap1_v2.pcapng -Y usb.capdata -T fields -e usb.capdata > capdata.hex`

We'll convert the extracted hexadecimal data into a binary file:
`xxd -r -p capdata.hex capdata.bin`

Extract headers and their associated file data from the created binary:
``binwalk -Me capdata.bin``

I rename the extracted files directory to *capdata.bin.extracted*.

A listing of files within the directory:
```
┌──(kali㉿kali)-[~/Desktop/practice_ctfs/pcap1_v2]
└─$ ls -l capdata.bin.extracted 
total 4924
-rw-r--r-- 1 kali kali 2308195 Aug 23 00:39 1C.zip
-rw-r--r-- 1 kali kali 2731652 Jul 15  2019 pcap_chal.pcap
```

The zip file is just the compressed *pcap_chal.pcap*.

Opening the PCAP file with Wireshark reveals a suspicious amount of ICMP traffic. Let's zero in using `!(data.len==48) and icmp` as the display filter:
![image1](images/image1.png)

Bingo. Communication through ICMP traffic between the two hosts 192.168.55.203 and 192.168.55.187. Let's build the script.

I used the following command to isolate the communication data within the packet, and then used a hex->text converter website online to make this process a little easier:
`tshark -r pcap_chal.pcap -Y '!(data.len == 48) and icmp' -T fields -e data`

Person A: 192.168.55.203
Person B: 192.168.55.187

```
Person A: bro, what you up to?

Person B: n2mh
Person B: why?

Person A: you didn't send that thing yet

Person B: oh... well, not over this

Person A: if not this, then what?

Person B: let's use cryptcat instead

Person A: another thing to install?
Person A: man... no one can see this

Person B: still... rather use encryption
Person B: we need to pick a key to use

Person A: I know just the one
Person A: Angela Bennett uses it to log into the Bethesda Naval Hospital

Person B: What? Oh, that old thing?
Person B: Hang on, lemme look it up
Person B: okay, I found it. use the metasploit port to receive

Person A: listener is up. send it.

Person B: okay, it's sent 

Person A: hash is good
```

"Angela Bennett uses it to log into the Bethesda Naval Hospital"

What?

Definitely a reference I did not get. After some research and asking some... older... folks about where this reference may come from, the movie "*The Net*" seemed to be the answer.

A [clip](https://www.youtube.com/watch?v=TFpKuq_tuIU) of the movie on YouTube reveals the passcode Bennett uses: `BER5348833`

After looking at the network traffic around the tunneled conversation to build context, after Person A says "listener is up. send it", an encrypted TCP connection is established from Person B to Person A over port 4444. We can infer from the conversation that a file is being transferred over this connection.

Research suggests that the encryption they are using is symmetric. This is good for us because we seem to already have the key to decrypt the traffic. Our next task is to find the file that the two hosts are transferring. 

Export the TCP conversation's raw data:
`tshark -r pcap_chal.pcap -Y '(ip.addr == 192.168.55.187) && (ip.addr == 192.168.55.203) and tcp' -T fields -e data.data | tr -d '\n' | xxd -r -ps > rawtransferdata`

Open up a cryptcat connection using the symmetric key we found:
`cryptcat -l -k BER5348833 -p 4444 > ./revealed_file`

Transfer the raw data:
`nc 0.0.0.0 4444 < rawtransferdata`

Success. If we do a file on the revealed file:
```
┌──(kali㉿kali)-[~/Desktop/practice_ctfs/pcap1_v2/_c.b.e]
└─$ file revealed_file 
revealed_file: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=10e9d33fce367a29cfe8d74866c3cb474ec61172, for GNU/Linux 3.2.0, stripped
```

Interesting. An executable. Let's hit it with strings and see what we can find:
```
┌──(kali㉿kali)-[~/Desktop/practice_ctfs/pcap1_v2/_c.b.e]
└─$ strings revealed_file | wc -l
88
                                                                                 
┌──(kali㉿kali)-[~/Desktop/practice_ctfs/pcap1_v2/_c.b.e]
└─$ strings revealed_file        
/lib64/ld-linux-x86-64.so.2
libc.so.6
socket
exit
htons
connect
inet_ntop
puts
__stack_chk_fail
select
printf
calloc
strlen
__errno_location
read
memcpy
strcat
gethostbyname
close
sleep
__cxa_finalize
strerror
__libc_start_main
write
free
GLIBC_2.14
GLIBC_2.4
GLIBC_2.2.5
_ITM_deregisterTMCloneTable
__gmon_start__
_ITM_registerTMCloneTable
u3UH
No such H
host
dH34%(
Server cH
losed thH
e connecH
tion
Failed tH
o callocH
 membuffH
dH34%(
gf1j7_n_H
r6_bg_g0H
t_f4u_reH
3ug_qe@mH
1j_c@pc_H
n_f'f
PRIVMSG H
#flag :CH
[]A\A]A^A_
irc.cct
ERROR %s
Failed to get ip
Found the server at %s
Connected to my server!
CAP LS 302
USER cct2019 cct2019 irc.cct :realname
NICK cct
JOIN #flag
;*3$"
GCC: (GNU) 9.1.0
.shstrtab
.interp
.note.gnu.build-id
.note.ABI-tag
.gnu.hash
.dynsym
.dynstr
.gnu.version
.gnu.version_r
.rela.dyn
.rela.plt
.init
.text
.fini
.rodata
.eh_frame_hdr
.eh_frame
.init_array
.fini_array
.dynamic
.got
.got.plt
.data
.bss
.comment
```

Interesting. Seems to be connecting to some sort of server. IRC also seems to somehow be involved. 

Running the binary with Wireshark opened reveals an interesting set of packets:
```
┌──(kali㉿kali)-[~/Desktop/practice_ctfs/pcap1_v2/_c.b.e]
└─$ ./revealed_file           
ERROR No s�(��
```
![image2](images/image2.png)

A DNS query for irc.cct. OK. We can fix this. Let's add an entry into */etc/hosts* and rerun the program to see if we can catch that callback:
```
┌──(kali㉿kali)-[~/Desktop/practice_ctfs/pcap1_v2/_c.b.e]
└─$ echo '127.0.0.1 irc.cct' >> /etc/hosts

┌──(kali㉿kali)-[~/Desktop/practice_ctfs/pcap1_v2/_c.b.e]
└─$ ./revealed_file    
Found the server at 127.0.0.1
ERROR Connection refused
```

![image3](images/image3.png)

OK. Making progress. The traffic looks like it is trying to connect to a specific port. Because we didn't know about this beforehand and had not set it to be open, it obviously terminated the connection before it even started. Let's fix this by using netcat to open port 6667 on the loopback, and running the executable in another terminal:
```
┌──(kali㉿kali)-[~/Desktop/practice_ctfs/pcap1_v2/_c.b.e]
└─$ nc -l -p 6667 127.0.0.1          
CAP LS 302
USER cct2019 cct2019 irc.cct :realname
NICK cct
JOIN #flag
PRIVMSG #flag :CCT{h3's_a_pc@p_w1z@rd_th3re_h4s_g0t_to_6e_a_7w1st}
```

Right on.
